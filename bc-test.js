// Test with obscure identifiers


var sha256 = require('js-sha256').sha256;

class B {
  	constructor(index, data, prev){
  		this.i = index;
  		this.t = Math.floor(Date.now() );
  		this.d = data;
  		this.p = prev;
  		this.h = this.getHash();
  	}

  	getHash(){
		return sha256(JSON.stringify(this.d) + this.prev + this.i + this.t);
  	}

}

class C {
	constructor(){
		this.c = [];
	}

	addB(data){
  		let index = this.c.length;
  		let prev = this.c.length !== 0 ? this.c[this.c.length - 1].h : 0;
  		let b = new B(index, data, prev);
  		this.c.push(b);
  	}

  isValid(){
  	for(var i=0;i<this.c.length;i++){
  		if(this.c[i].h !== this.c[i].getHash())
  			return false;
  		if(i > 0 && this.c[i].prev !== this.c[i-1].h)
  			return false;
  	}
  	return true;
  }

}


// ------------
const myC = new C();

myC.addB({sender: "Markus", reciever: "Jessi", amount: 100});
myC.addB({sender: "Dani", reciever: "Alina", amount: 50});
myC.addB({sender: "Banu", reciever: "Chris", amount: 75});

console.log(JSON.stringify(myC, null, 4));
console.log("C valid : " + myC.isValid()) ;
//-------


console.log("now we hack a b.... Nils wants to get some bucks, as well!!!");
myC.c[1].d.reciever = "Nils"
console.log("myBC.c[1].d.reciever = Nils");
console.log("let's have a look at the changed B")
console.log(JSON.stringify(myC.c[1], null, 4));
console.log("C valid : " + myC.isValid()) ;
