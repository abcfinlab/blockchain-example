// wir benutzen sha256 als Hash-Funktion fuer die Blöcke...
var sha256 = require('js-sha256').sha256;

// class fuer einen Block
class Block {
  // der Block wird mit index, data, timestamp
  // und den letzten und eigenen Hash initialisiert
  constructor(index, data, prevHash){
  	this.index = index;
  	this.timestamp = Math.floor(Date.now() );
  	this.data = data;
  	this.prevHash = prevHash;
  	this.hash = this.getHash();
  }

  //hier berechnen wir den eigenen Hash aus dem Inhalt
  getHash(){
  	return sha256(JSON.stringify(this.data) + this.prevHash + this.index + this.timestamp);
  }
}

// noch eine class fuer die Chain
class BlockChain{
	constructor(){
		this.chain = [];
	}
  // hier fügen wir einen Block an
  addBlock(data){
  	let index = this.chain.length;
    // ach ja: der initiale Block hat als prevHash eine Null.
  	let prevHash = this.chain.length !== 0 ? this.chain[this.chain.length - 1].hash : 0;
  	let block = new Block(index, data, prevHash);

  	this.chain.push(block);
  }

  // hier checken wir durch selbst berechnen und vergleichen
  // die Authenzität der Kette
  chainIsValid(){
  	for(var i=0;i<this.chain.length;i++){
  		if(this.chain[i].hash !== this.chain[i].getHash())
  			return false;
  		if(i > 0 && this.chain[i].prevHash !== this.chain[i-1].hash)
  			return false;
  	}

  	return true;
  }
}

//-------------------------------------
// hier probieren wir mal alles aus.
const myBC = new BlockChain();

// ein paar Inhalte fuer die Kette....
myBC.addBlock({sender: "Markus", reciever: "Jessi", amount: 100});
myBC.addBlock({sender: "Dani", reciever: "Alina", amount: 50});
myBC.addBlock({sender: "Banu", reciever: "Chris", amount: 75});

// einmal ausgeben
console.log(JSON.stringify(myBC, null, 4));
// einmal checken, ob alles stimmt
console.log("Chain valid : " + myBC.chainIsValid()) ;
//-------
// jetzt manipulieren wir die Kette, wodurch diese ungültig wird....
console.log("now we hack a block.... Nils wants to get some bucks, as well!!!");
// ... Alina hat genug Geld ... ;-)
myBC.chain[1].data.reciever = "Nils"
console.log("myBC.chain[1].data.reciever = Nils");
console.log("let's have a look at the changed block")
console.log(JSON.stringify(myBC.chain[1], null, 4));
console.log("Chain valid : " + myBC.chainIsValid()) ;
